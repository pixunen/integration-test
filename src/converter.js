/**
 * Padding output 2 characters allways
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters 
 */


// const pad = (hex) => {
//     return (hex.length === 1 ? "0" + hex : hex);
// }

module.exports = {
    hexToRgb: (red, green, blue) => {
        const redHex = parseInt(red, 16);
        const greenHex = parseInt(green, 16);
        const blueHex = parseInt(blue, 16);

        const rgb = `${redHex},${greenHex},${blueHex}`;
        return rgb;
    }
}