// TDD - Unit Testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.hexToRgb('ff',00,00); // red 255,0,0
            const greenHex = converter.hexToRgb(00,'ff',00); // green 0,255,0
            const blueHex = converter.hexToRgb(00,00,'ff'); // blue 0,0,255

            expect(redHex).to.equal("255,0,0");
            expect(greenHex).to.equal("0,255,0");
            expect(blueHex).to.equal("0,0,255");
        });
    });
});