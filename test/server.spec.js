// Integration test

const expect = require("chai").expect;
const request = require("request");
const { response } = require("../src/server");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {
    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        })
    });

    describe("RGB to Hex conversion", () => {
        const url = `http://localhost:${port}/hex-to-rgb?red=ff&green=00&blue=00`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in rgb", (done) => {
            request(url, (error,  response, body) => {
                expect(body).to.equal("25500");
                done();
            })
        })
    });

    after("Stop server after tests", (done) =>{
        server.close();
        done();
    });
});